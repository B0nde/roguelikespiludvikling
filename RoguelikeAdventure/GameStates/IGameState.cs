﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace RoguelikeAdventure.GameStates
{
    internal interface IGameState
    {
        void Initialize();
        void Update();
        void Draw(SpriteBatch sb);
    }
}
