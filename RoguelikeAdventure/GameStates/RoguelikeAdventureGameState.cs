﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels;
using RoguelikeAdventure.Levels.Entities;
using RoguelikeAdventure.Levels.Rooms;

namespace RoguelikeAdventure.GameStates
{
    internal class RoguelikeAdventureGameState : IGameState
    {
        private Level level;

        public void Initialize()
        {
            var startRoom = new TestRoom();
            startRoom.AddEntity(new PlayerEntity(new Vector2(32, 32)));

            level = new Level(new Room[,]
            {
                {startRoom, new TestRoom(), new TestRoom(), new TestRoom()},
                {new TestRoom(), new TestRoom(), new TestRoom(), new TestRoom()},
                {new TestRoom(), new TestRoom(), new TestRoom(), new TestRoom()},
                {new TestRoom(), new TestRoom(), new TestRoom(), new TestRoom()}
            });
        }

        public void Update()
        {
            level.Update();
        }

        public void Draw(SpriteBatch sb)
        {
            level.Draw(sb);
        }
    }
}
