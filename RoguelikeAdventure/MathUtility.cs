﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoguelikeAdventure
{
    internal class MathUtility
    {
        internal static float SineFunction(float time, float amplitude, float fase, float freqence)
        {
            return amplitude*(float) Math.Sin(2*Math.PI*freqence*time + fase);
        }
    }
}
