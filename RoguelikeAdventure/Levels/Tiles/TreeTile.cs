﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Entities;

namespace RoguelikeAdventure.Levels.Tiles
{
    internal class TreeTile : Tile
    {
        internal TreeTile(int id) : base(id)
        {
        }

        internal override void Draw(SpriteBatch sb, Rectangle dst)
        {
            base.Draw(sb, dst);

            Tile.GrassTile.Draw(sb, dst);
            sb.Draw(Game.Sprites, dst, new Rectangle(16, 0, 16, 16), Color.White);
        }

        internal override bool Blocks(Entity e)
        {
            return true;
        }
    }
}
