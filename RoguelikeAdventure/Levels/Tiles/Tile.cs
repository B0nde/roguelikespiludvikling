﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Entities;
using RoguelikeAdventure.Levels.Rooms;

namespace RoguelikeAdventure.Levels.Tiles
{
    internal class Tile
    {
        internal const int TileSize = Room.TileSize;

        private static readonly Tile[] tiles = new Tile[6];
        internal static readonly Tile VoidTile = new Tile(0);
        internal static readonly GrassTile GrassTile = new GrassTile(1);
        internal static readonly StoneTile StoneTile = new StoneTile(2);
        internal static readonly SandTile SandTile = new SandTile(3);
        internal static readonly TreeTile TreeTile = new TreeTile(4);
        internal static readonly RedBrickTile RedBrickTile = new RedBrickTile(5);

        internal int Id { get; private set; }

        protected Tile(int id)
        {
            Id = id;

            if(tiles[Id] != null) throw new Exception("Duplicated til ids!");

            tiles[Id] = this;
        }

        internal virtual void Draw(SpriteBatch sb, Rectangle dst)
        {
        }

        internal virtual bool Blocks(Entity e)
        {
            return false;
        }

        internal virtual void OnCollision(Entity e)
        {
        }

        internal static Tile GetTile(int id)
        {
            return tiles[id];
        }
    }
}
