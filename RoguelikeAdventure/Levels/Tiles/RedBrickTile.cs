﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Entities;

namespace RoguelikeAdventure.Levels.Tiles
{
    internal class RedBrickTile : Tile
    {
        internal RedBrickTile(int id) : base(id)
        {
        }

        internal override void Draw(SpriteBatch sb, Rectangle dst)
        {
            base.Draw(sb, dst);

            sb.Draw(Game.Sprites, dst, new Rectangle(32, 0, 16, 16), Color.DarkRed);
        }

        internal override bool Blocks(Entity e)
        {
            return !(e is DoorEntity);
        }
    }
}
