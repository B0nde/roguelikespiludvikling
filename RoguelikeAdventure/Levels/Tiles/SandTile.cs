﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RoguelikeAdventure.Levels.Tiles
{
    internal class SandTile : Tile
    {
        internal SandTile(int id) : base(id)
        {
        }

        internal override void Draw(SpriteBatch sb, Rectangle dst)
        {
            base.Draw(sb, dst);

            sb.Draw(Game.Sprites, dst, new Rectangle(0, 0, 16, 16), Color.LightYellow);
        }
    }
}
