﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Entities;
using RoguelikeAdventure.Levels.Physics;
using RoguelikeAdventure.Levels.Tiles;

namespace RoguelikeAdventure.Levels.Rooms
{
    internal class Room
    {
        internal const int TileSize = 64;

        internal Level Level { get; set; }
        internal List<Entity> Entities { get; private set; }
        internal int[,] Tiles { get; private set; }

        private List<Entity> newEntities; 

        internal Room(int width, int height)
        {
            Entities = new List<Entity>();
            newEntities = new List<Entity>();

            Tiles = new int[width, height];
        }

        internal void Update()
        {
            Entities.AddRange(newEntities);
            newEntities.Clear();

            for (int i = 0; i < Entities.Count; i++)
            {
                if(Entities[i].Active)
                    Entities[i].Update();
                else
                {
                    Entities.RemoveAt(i);
                    i--;
                }
            }

            // Resolve collisions with Entities.
            for(int i = 0; i < Entities.Count; i++)
                for (int j = i + 1; j < Entities.Count; j++)
                {
                    var contact = Entities[i].Intersects(Entities[j]);

                    if (contact.Intersects)
                    {
                        Entities[i].OnCollision(Entities[j]);
                        Entities[j].OnCollision(Entities[i]);
                    }
                        
                    if (Entities[j].Blocks(Entities[i]))
                        Entities[i].ResolveCollision(contact);
                }

            // Resolve collisions with Tiles;
            for (int i = 0; i < Entities.Count; i++)
            {
                var tileX = (int) (Entities[i].Position.X/TileSize);
                var tileY = (int) (Entities[i].Position.Y/TileSize);

                ResolveTileCollision(tileX, tileY, Entities[i]);
                ResolveTileCollision(tileX, tileY + 1, Entities[i]);
                ResolveTileCollision(tileX + 1, tileY + 1, Entities[i]);
                ResolveTileCollision(tileX + 1, tileY, Entities[i]);
                ResolveTileCollision(tileX, tileY - 1, Entities[i]);
                ResolveTileCollision(tileX - 1, tileY - 1, Entities[i]);
                ResolveTileCollision(tileX - 1, tileY, Entities[i]);
            }
        }

        private void ResolveTileCollision(int tileX, int tileY, Entity e)
        {
            if (tileX >= 0 && tileY >= 0 && tileX < Tiles.GetLength(0) && tileY < Tiles.GetLength(1))
            {
                if (Tile.GetTile(Tiles[tileX, tileY]).Blocks(e))
                {
                    var aabb = new Aabb(new Vector2(tileX * TileSize, tileY * TileSize), new Vector2(TileSize, TileSize));
                    var contact = e.Intersects(aabb);

                    if(contact.Intersects)
                        Tile.GetTile(Tiles[tileX, tileY]).OnCollision(e);

                    e.ResolveCollision(contact);
                }
            }
        }

        internal void Draw(SpriteBatch sb)
        {
            for(int i = 0; i < Tiles.GetLength(0); i++)
                for (int j = 0; j < Tiles.GetLength(1); j++)
                {
                    var position = Level.GetCameraPositioned(new Vector2(i * TileSize, j * TileSize));
                    Tile.GetTile(Tiles[i, j]).Draw(sb, new Rectangle((int)position.X, (int)position.Y, TileSize, TileSize));
                }

            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Draw(sb);
            }
        }

        internal void AddEntity(Entity e)
        {
            newEntities.Add(e);
            e.Room = this;
        }
    }
}
