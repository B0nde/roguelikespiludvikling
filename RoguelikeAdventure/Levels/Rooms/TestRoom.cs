﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RoguelikeAdventure.Levels.Entities;
using RoguelikeAdventure.Levels.Tiles;

namespace RoguelikeAdventure.Levels.Rooms
{
    internal class TestRoom : Room
    {
        internal TestRoom() : base(10, 5)
        {
            for (int i = 0; i < Tiles.GetLength(0); i++)
                Tiles[i, 0] = Tile.RedBrickTile.Id;

            for (int i = 0; i < Tiles.GetLength(0); i++)
                Tiles[i, Tiles.GetLength(1) - 1] = Tile.RedBrickTile.Id;

            for (int i = 0; i < Tiles.GetLength(1); i++)
                Tiles[0, i] = Tile.RedBrickTile.Id;

            for (int i = 0; i < Tiles.GetLength(1); i++)
                Tiles[Tiles.GetLength(0) - 1, i] = Tile.RedBrickTile.Id;

            for(int i = 1; i < Tiles.GetLength(0) - 1; i++)
                for (int j = 1; j < Tiles.GetLength(1) - 1; j++)
                {
                    Tiles[i, j] = Tile.StoneTile.Id;
                }

            var rand = new Random();

            for(int i = 0; i < 3; i++)
                AddEntity(new MobEntity(new Vector2((float)rand.NextDouble() * TileSize * 7 + TileSize, (float)rand.NextDouble() * TileSize * 2 + TileSize)));

            AddEntity(new DoorEntity(new Vector2(8*Tile.TileSize, 2 * Tile.TileSize), 1, 0));
        }
    }
}
