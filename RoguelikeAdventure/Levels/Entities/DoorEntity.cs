﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Tiles;

namespace RoguelikeAdventure.Levels.Entities
{
    internal class DoorEntity : Entity
    {
        private int roomX, roomY;

        internal DoorEntity(Vector2 position, int roomX, int roomY) : base(position, new Vector2(Tile.TileSize, Tile.TileSize))
        {
            this.roomX = roomX;
            this.roomY = roomY;
        }

        internal override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);

            var pos = Room.Level.GetCameraPositioned(Position);
            sb.Draw(Game.Sprites, new Rectangle((int)pos.X, (int)pos.Y, (int)Size.X, (int)Size.Y), new Rectangle(48, 0, 16, 16), Color.White);
        }

        internal override void OnCollision(Entity e)
        {
            base.OnCollision(e);

            if (e is PlayerEntity)
            {
                Room.Entities.Remove(e);
                Room.Level.SetRoom(roomX, roomY);
                Room.Level.Rooms[roomX, roomY].AddEntity(e);
            }
        }
    }
}
