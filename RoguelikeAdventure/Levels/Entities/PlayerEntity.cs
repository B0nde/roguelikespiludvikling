﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RoguelikeAdventure.Levels.Entities
{
    internal class PlayerEntity : Entity
    {
        private const float Friction = 0.9f;
        private const float Speed = 0.5f;

        private float deltaX;
        private float deltaY;

        internal PlayerEntity(Vector2 position) : base(position, new Vector2(32, 64))
        {
        }

        internal override void Update()
        {
            var pos = Room.Level.GetCameraPositioned(Position);

            base.Update();

            deltaX *= Friction;
            deltaY *= Friction;

            if (Keyboard.GetState().IsKeyDown(Keys.A))
                deltaX -= Speed;

            if (Keyboard.GetState().IsKeyDown(Keys.D))
                deltaX += Speed;

            if (Keyboard.GetState().IsKeyDown(Keys.W))
                deltaY -= Speed;

            if (Keyboard.GetState().IsKeyDown(Keys.S))
                deltaY += Speed;

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                Room.AddEntity((new Lazer(Position+new Vector2(28,10),1)));
            else if (Keyboard.GetState().IsKeyDown(Keys.Left))
                Room.AddEntity((new Lazer(Position + new Vector2(-18, 10), -1)));

            Position = Position + new Vector2(deltaX, deltaY);
            Room.Level.CameraPosition = Position + Size / 2 - new Vector2(400, 300);
        }

        internal override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);

            var pos = Room.Level.GetCameraPositioned(Position);
            sb.Draw(Game.Sprites, new Rectangle((int)pos.X, (int)pos.Y, (int)Size.X, (int)Size.Y), new Rectangle(0, 16, 16, 32), Color.White);
        }

    }
}
