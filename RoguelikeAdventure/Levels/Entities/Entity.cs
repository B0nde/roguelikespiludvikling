﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Physics;
using RoguelikeAdventure.Levels.Rooms;

namespace RoguelikeAdventure.Levels.Entities
{
    internal class Entity : Aabb
    {
        internal bool Active { get; set; }
        internal Room Room { get; set; }

        internal Entity(Vector2 position, Vector2 size) : base(position, size)
        {
            Active = true;
        }

        internal virtual void Update()
        {
            
        }

        internal virtual void Draw(SpriteBatch sb)
        {
            
        }

        internal virtual void OnCollision(Entity e)
        {
            
        }

        internal virtual bool Blocks(Entity e)
        {
            return false;
        }
    }
}
