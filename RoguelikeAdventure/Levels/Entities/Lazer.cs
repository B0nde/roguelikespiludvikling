﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RoguelikeAdventure.Levels.Entities
{
    class Lazer : Entity
    {
        private const ulong Time = 6000*1000;
        private const float Length = 200;

        private ulong time;
        private Vector2 startPoint;
        private int maskX;

        internal Lazer(Vector2 position, int maskX) : base(position, new Vector2(16,16))
        {
            time = (ulong)DateTime.Now.Ticks;
            this.startPoint = position;
            this.maskX = maskX;
        }

        internal override void Update()
        {
            base.Update();

            float t = ((ulong)DateTime.Now.Ticks - time)/(float)Time;


            Position = startPoint+new Vector2(t*Length*maskX,MathUtility.SineFunction(t,10,0,1));

            if (t >= 1)
                Active = false;
        }

        internal override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);

            var pos = Room.Level.GetCameraPositioned(Position);

            sb.Draw(Game.Sprites, new Rectangle((int)pos.X, (int)pos.Y, (int)Size.X, (int)Size.Y), new Rectangle(62, 0, 16, 16), Color.White);
        }

        internal override void OnCollision(Entity e)
        {
            base.OnCollision(e);

            if (e is MobEntity)
            {
                e.Active = false;
            }

            
        }
    }
}
