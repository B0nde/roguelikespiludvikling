﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RoguelikeAdventure.Levels.Entities
{
    internal class MobEntity : Entity
    {
        private const float Speed = 1.25f;

        private enum State { Roamer, Attack }

        private State state;
        private Entity target;
        private float deltaX;
        private float deltaY;

        internal MobEntity(Vector2 position) : base(position, new Vector2(32, 64))
        {
            state = State.Roamer;
        }

        internal override void Update()
        {
            base.Update();

            switch (state)
            {
                case State.Roamer:
                    UpdateRoam();
                    break;

                case State.Attack:
                    UpdateAttack();
                    break;
            }
        }

        private void UpdateRoam()
        {
            var rand = new Random();
            if (rand.Next(20) == 0)
            {
                deltaX += (float) rand.NextDouble()*Speed*2f - Speed;
                deltaY += (float) rand.NextDouble()*Speed*2f - Speed;
            }

            Position = Position + new Vector2(deltaX, deltaY);

            Entity nearest = Room.Entities[0];
            float dist = float.MaxValue;

            foreach (var e in Room.Entities)
            {
                if (!e.Equals(this) && e is PlayerEntity)
                {
                    var d = Vector2.Distance(Position, e.Position);

                    if (d < dist)
                    {
                        dist = d;
                        nearest = e;
                    }
                }
            }

            if (dist < 64)
            {
                target = nearest;
                state = State.Attack;
            }
        }

        private void UpdateAttack()
        {
            if(!target.Active) state = State.Roamer;

            //if (target.Active)
            //{
            //    var delta = target.Position - Position;


            //    Room.AddEntity((new Lazer(Position + new Vector2(-18, 10), delta.X < 0 ? -1:1)));
            //}

            var dif = target.Position - Position;
            dif.Normalize();

            Position += Speed*dif;
        }

        internal override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);

            var pos = Room.Level.GetCameraPositioned(Position);
            sb.Draw(Game.Sprites, new Rectangle((int)pos.X, (int)pos.Y, (int)Size.X, (int)Size.Y), new Rectangle(0, 16, 16, 32), Color.White);
        }
    }
}
