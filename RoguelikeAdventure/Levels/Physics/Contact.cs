﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RoguelikeAdventure.Levels.Physics
{
    internal class Contact
    {
        public Vector2 Normal { get; set; }
        public float Distance { get; set; }
        public bool Intersects { get; set; }

        public override string ToString()
        {
            return "Contact: \nNormal = " + Normal + "\nDistance = " + Distance;
        }
    }
}
