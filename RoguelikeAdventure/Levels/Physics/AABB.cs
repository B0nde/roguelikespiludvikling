﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RoguelikeAdventure.Levels.Physics
{
    internal class Aabb
    {
        public Vector2 Position { get; set; }
        public Vector2 Size { get; set; }

        internal Aabb(Vector2 position, Vector2 size)
        {
            Position = position;
            Size = size;
        }

        internal void ResolveCollision(Contact contact)
        {
            if (contact.Intersects)
                Position += contact.Normal*contact.Distance;
        }

        internal Contact Intersects(Aabb aabb2)
        {
            var minA = Position;
            var maxA = Position + Size;
            var minB = aabb2.Position;
            var maxB = aabb2.Position + aabb2.Size;

            /*Console.WriteLine();
            Console.WriteLine("MinA = " + minA);
            Console.WriteLine("MaxA = " + maxA);
            Console.WriteLine("MinB = " + minB);
            Console.WriteLine("MaxB = " + maxB);*/

            var dist = float.MaxValue;
            var normal = new Vector2();

            if(!TestAxis(new Vector2(1, 0), minA.X, maxA.X, minB.X, maxB.X, ref dist, ref normal))
                return new Contact();

            if (!TestAxis(new Vector2(0, 1), minA.Y, maxA.Y, minB.Y, maxB.Y, ref dist, ref normal))
                return new Contact();

            dist = (float)Math.Sqrt(dist) * 1.00001f;
            normal.Normalize();

            return new Contact(){Distance = dist, Intersects = true, Normal = normal};
        }

        private static bool TestAxis(Vector2 axis, float minA, float maxA, float minB, float maxB, ref float dist, ref Vector2 normal)
        {
            var axisLength = axis.LengthSquared();

            if (axisLength < 1.0e-8f)
                return false;

            var d1 = maxB - minA;
            var d2 = maxA - minB;

            if (d1 <= 0.0f || d2 <= 0.0f)
                return false;

            var d = d1 < d2 ? d1 : -d2;
            var seperation = axis * (d / axisLength);
            var seperationLength = seperation.LengthSquared();

            if (seperationLength < dist)
            {
                dist = seperationLength;
                normal = seperation;
            }

            return true;
        }
    }
}
