﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoguelikeAdventure.Levels.Rooms;

namespace RoguelikeAdventure.Levels
{
    internal class Level
    {
        internal Room[,] Rooms { get; private set; }
        internal Vector2 CameraPosition { get; set; }

        private int roomX, roomY;

        internal Level(Room[,] rooms)
        {
            Rooms = rooms;

            for(int i = 0; i < Rooms.GetLength(0); i++)
                for (int j = 0; j < Rooms.GetLength(1); j++)
                {
                    Rooms[i, j].Level = this;
                }
        }

        internal void Update()
        {
            Rooms[roomX, roomY].Update();
        }

        internal void Draw(SpriteBatch sb)
        {
            Rooms[roomX, roomY].Draw(sb);
        }

        internal void SetRoom(int x, int y)
        {
            roomX = x;
            roomY = y;
        }

        internal Vector2 GetCameraPositioned(Vector2 position)
        {
            return position - CameraPosition;
        }
    }
}
