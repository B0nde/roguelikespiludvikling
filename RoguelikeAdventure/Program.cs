﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace RoguelikeAdventure
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Game())
                game.Run();
        }
    }
#endif
}
