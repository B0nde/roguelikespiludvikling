﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RoguelikeAdventure.GameStates;

namespace RoguelikeAdventure
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        internal static Texture2D Sprites { get; private set; }

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private IGameState gameState;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            Window.Title = "Roguelike Adventure";

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            ChangeGameState(new RoguelikeAdventureGameState());

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Sprites = Content.Load<Texture2D>("Sprites.png");
        }

        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            gameState.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            gameState.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        internal void ChangeGameState(IGameState gameState)
        {
            this.gameState = gameState;
            this.gameState.Initialize();
        }
    }
}
